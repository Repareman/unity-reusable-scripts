﻿#region Disclaimer

// -----------------------------------------------------------------------
// <copyright file="DisplayTextDemo.cs">
// Copyright (c) 2016, Sam Bolton
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
// IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// -----------------------------------------------------------------------

#endregion

namespace ReusableScripts.CustomAttributes
{
    using UnityEngine;

    /// <summary>
    ///     Demos the use of the display text custom attribute and override method.
    /// </summary>
    /// <seealso cref="MonoBehaviour" />
    public class DisplayTextDemo : MonoBehaviour
    {
        #region  Private Methods

        /// <summary>
        ///     Called on the frame when a script is enabled just before any of the Update methods is called the first time.
        /// </summary>
        private void Start()
        {
            // See the difference between the 'ToString' and 'ToDisplay' calls for the "None" enum.
            Debug.LogFormat(
                "ToString: {0}\nToDisplay: {1}",
                DisplayTestEnum.None.ToString(),
                DisplayTestEnum.None.ToDisplay());

            // See the difference between the 'ToString' and 'ToDisplay' calls for the "ActualDisplay" enum.
            Debug.LogFormat(
                "ToString: {0}\nToDisplay: {1}",
                DisplayTestEnum.ActualDisplay.ToString(),
                DisplayTestEnum.ActualDisplay.ToDisplay());

            // See the difference between the 'ToString' and 'ToDisplay' calls for the "OverwrittenDisplay" enum.
            Debug.LogFormat(
                "ToString: {0}\nToDisplay: {1}",
                DisplayTestEnum.OverwrittenDisplay.ToString(),
                DisplayTestEnum.OverwrittenDisplay.ToDisplay());
        }

        #endregion
    }
}