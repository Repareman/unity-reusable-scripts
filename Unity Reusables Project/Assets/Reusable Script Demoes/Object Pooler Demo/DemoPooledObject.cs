﻿#region Disclaimer

// -----------------------------------------------------------------------
// <copyright file="DemoPooledObject.cs">
// Copyright (c) 2016, Sam Bolton
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
// IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// -----------------------------------------------------------------------

#endregion

namespace ReusableScripts.ObjectPooler.Demo
{
    using System.Collections;
    using UnityEngine;

    /// <summary>
    ///     Demos the functionality and implementation of a pooled object.
    /// </summary>
    /// <seealso cref="ObjectPooler.PooledObject{DemoPool}" />
    public class DemoPooledObject : PooledObject<DemoPool>
    {
        #region Private Field

        /// <summary>
        ///     The maximum number of seconds before this object deactivates.
        /// </summary>
        [SerializeField]
        private float _maximumSecondsToDeactivate = 3.0f;

        /// <summary>
        ///     The minimum number of seconds before this object deactivates.
        /// </summary>
        [SerializeField]
        private float _minimumSecondsToDeactivate = 1.5f;

        #endregion

        #region PooledObject<DemoPool> Overriden Protected Methods

        /// <summary>
        ///     Method that will run after the object is deactivated.
        /// </summary>
        protected override void PostDeactivation()
        { }

        /// <summary>
        ///     Method that will run before the object is deactivated.
        /// </summary>
        protected override void PreDeactivation()
        { }

        #endregion

        #region  Private Methods

        /// <summary>
        ///     Waits the time to deactivate and then calls the deactivation.
        /// </summary>
        /// <returns>An IEnumerator for the coroutine to function.</returns>
        private IEnumerator CountdownToDeactivation()
        {
            yield return new WaitForSeconds(Random.Range(_minimumSecondsToDeactivate, _maximumSecondsToDeactivate));
            DeactivateObject();
        }

        /// <summary>
        ///     Called when the object becomes enabled and active.
        /// </summary>
        private void OnEnable()
        {
            StartCoroutine(CountdownToDeactivation());
        }

        #endregion
    }
}