﻿#region Disclaimer

// -----------------------------------------------------------------------
// <copyright file="DemoPool.cs">
// Copyright (c) 2016, Sam Bolton
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
// IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// -----------------------------------------------------------------------

#endregion

namespace ReusableScripts.ObjectPooler.Demo
{
    using UnityEngine;

    public class DemoPool : ObjectPool<DemoPool>
    {
        #region Private Field

        /// <summary>
        ///     The transform of the object that will act as the parent of the active items.
        /// </summary>
        [SerializeField]
        private Transform _activeParent = null;

        /// <summary>
        ///     The prefab that will be used as the pooled object.
        /// </summary>
        [SerializeField]
        private GameObject _objectPrefab = null;

        /// <summary>
        ///     Whether we should wait to get the objects until the space bar is pressed.
        /// </summary>
        [SerializeField]
        private bool _waitForSpacebar = false;

        #endregion

        #region Public Property

        /// <summary>
        ///     Gets or sets the parent of the object when it is active in the scene.
        /// </summary>
        public override Transform ActiveParent
        {
            get
            {
                return _activeParent;
            }
            set
            {
                _activeParent = value;
            }
        }

        #endregion

        #region Protected Property

        /// <summary>
        ///     The name of the game object that will be generated.
        /// </summary>
        protected override string _gameObjectName
        {
            get
            {
                return "Demo Pool";
            }
        }

        /// <summary>
        ///     Gets a value indicating whether this pool is allowed to grow or not.
        /// </summary>
        protected override bool _grow
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        ///     Gets the maximum number of pooled objects that should be allowed to exist in the pool at
        ///     any given moment in time.
        /// </summary>
        /// <remarks>
        ///     Set to 0 if the maximum is infinite.
        /// </remarks>
        protected override int _max
        {
            get
            {
                return 100;
            }
        }

        /// <summary>
        ///     Gets the minimum number of pooled objects that should exist in the pool at any given
        ///     moment in time.
        /// </summary>
        /// <remarks>
        ///     If the minimum is 0, then no objects will be made at the beginning of the scene.
        /// </remarks>
        protected override int _min
        {
            get
            {
                return 5;
            }
        }

        #endregion

        #region ObjectPool<DemoPool> Overriden Protected Methods

        /// <summary>
        ///     Sets the pooled object to its active state.
        /// </summary>
        /// <param name="pooledObject">The pooled object.</param>
        protected override void ActivatePooledObject(GameObject pooledObject)
        {
            pooledObject.transform.SetParent(_activeParent);
            pooledObject.transform.localPosition = Vector3.zero;
            pooledObject.SetActive(true);
        }

        /// <summary>
        ///     Called during the awake function.
        /// </summary>
        /// <remarks>
        ///     Use this instead of Awake so that the singleton can remain to operate correctly.
        /// </remarks>
        protected override void SingletonAwake()
        {
            base.SingletonAwake();
            _pooledObjectPrefab = _objectPrefab;
        }

        #endregion

        #region  Private Methods

        /// <summary>
        ///     Called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        private void Update()
        {
            if (!Input.GetKeyDown(KeyCode.Space) && _waitForSpacebar)
            {
                return;
            }

            // Get all the objects up to the max value
            int numberOfInactiveObjectsToGrab = _max - GetAllActivePooledObjects().Length;
            GetAndActivatePooledObjects(numberOfInactiveObjectsToGrab);
        }

        #endregion
    }
}