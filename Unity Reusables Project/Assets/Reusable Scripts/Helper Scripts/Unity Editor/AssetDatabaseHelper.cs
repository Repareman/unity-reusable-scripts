﻿#region Disclaimer

// // -----------------------------------------------------------------------
// // <copyright file="AssetDatabaseHelper.cs">
// // Copyright (c) 2016, Sam Bolton
// // All rights reserved.
// // Redistribution and use in source and binary forms, with or without
// // modification, are permitted provided that the following conditions are met:
// // 1. Redistributions of source code must retain the above copyright notice,
// // this list of conditions and the following disclaimer.
// // 2. Redistributions in binary form must reproduce the above copyright notice,
// // this list of conditions and the following disclaimer in the documentation
// // and/or other materials provided with the distribution.
// // THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// // AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// // IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// // ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// // LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// // DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// // LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// // THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// // NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
// // IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// // </copyright>
// // -----------------------------------------------------------------------

#endregion

namespace ReusableScripts.HelperScripts.UnityEditor
{
    using System.IO;
    using System.Linq;
    using System.Text;
    using global::UnityEditor;
    using UnityEngine;

    /// <summary>
    ///     Provides helper methods for assistance in the asset database functionality.
    /// </summary>
    public static class AssetDatabaseHelper
    {
        #region  Public Methods

        /// <summary>
        ///     Creates a new asset at the given path with the given file name.
        ///     Make sure that the path uses a supported extension!
        /// </summary>
        /// <param name="asset">The asset that we're creating.</param>
        /// <param name="path">The folder path where the asset will be created in.</param>
        /// <param name="fileName">Name of the file with the extension.</param>
        /// <param name="generateUniqueName">If set to <c>true</c> then the file name will be changed to be unique.</param>
        /// <param name="createFolderPath">
        ///     If set to <c>true</c> then the given path will be created if it doesn't exist and is
        ///     valid.
        /// </param>
        /// <example>
        ///     The following code will show an example of how to use the CreateAsset method:
        ///     <code>
        /// public static RandomClass()
        /// {
        ///     private static void Init()
        ///     {
        ///         AssetDatabaseHelper.CreateAsset(createdAsset, "Assets/Storage Location", "New Asset Name.asset", true, true);
        ///     }
        /// }
        /// </code>
        /// </example>
        /// <seealso cref="AssetDatabase.CreateAsset" />
        public static void CreateAsset(
            Object asset,
            string path,
            string fileName,
            bool generateUniqueName = false,
            bool createFolderPath = false)
        {
            // Make sure that we were not passed a null asset
            if (asset == null)
            {
                Debug.LogError("Can not create an asset of a null type.");
                return;
            }

            // Make sure that we were not passed a null path
            if (path == null)
            {
                Debug.LogError("Can not create an asset at a null path.");
                return;
            }

            // Make sure that the file name is not null
            if (fileName == null)
            {
                Debug.LogError("Can not create an asset without a file name.");
                return;
            }

            // Make sure that the file name is valid
            if (Path.GetInvalidFileNameChars().Any(fileName.Contains))
            {
                Debug.LogErrorFormat(
                    "Can not create a file with invalid file name characters.\nFile Name: {0}",
                    fileName);
                return;
            }

            // Validate that the folder path exists
            if (!AssetDatabase.IsValidFolder(path))
            {
                if (createFolderPath)
                {
                    CreateFolderStructure(path);
                }
                else
                {
                    Debug.LogError("The path provided was not valid.");
                    return;
                }
            }

            // Build the final file path
            string finalFilePath = generateUniqueName
                ? AssetDatabase.GenerateUniqueAssetPath(string.Format("{0}/{1}", path, fileName))
                : string.Format("{0}/{1}", path, fileName);

            // Create the asset at the designated path with the given/generated name
            AssetDatabase.CreateAsset(asset, finalFilePath);
        }

        /// <summary>
        ///     Creates a folder structure in the asset database.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <remarks>The path must begin with "Assets". If it does not then this will error and not create any folders.</remarks>
        /// <example>
        ///     This sample will show a proper execution of calling this method.
        ///     <code>
        /// public class RandomClass()
        /// {
        ///     private static void InitFolders()
        ///     {
        ///         CreateFolderStructure("Assets/FolderPath/Sub Folders/Supported");
        ///     }
        /// }
        /// </code>
        /// </example>
        /// <seealso cref="AssetDatabase.CreateFolder" />
        public static void CreateFolderStructure(string path)
        {
            // Check the path for invalid characters
            if (Path.GetInvalidPathChars().Any(path.Contains))
            {
                Debug.LogErrorFormat(
                    "Can not create a folder with a path that contains invalid characters.\nPath: {0}",
                    path);
            }

            // Make sure that we're existing inside of the 'Assets' folder
            if (!path.StartsWith("Assets"))
            {
                Debug.LogErrorFormat(
                    "Can not create a folder structure that does not begin with 'Assets'.\nPath: {0}",
                    path);
            }

            // Create the variables needed to begin building the child items
            StringBuilder currentFolder = new StringBuilder("Assets");
            string[] folders = path.Split('/');

            // Loop through the folders and create them in the asset database
            for (int i = 1; i < folders.Length; i++)
            {
                if (!AssetDatabase.IsValidFolder(string.Format("{0}/{1}", currentFolder, folders[i])))
                {
                    AssetDatabase.CreateFolder(currentFolder.ToString(), folders[i]);
                }

                currentFolder.AppendFormat("/{0}", folders[i]);
            }
        }

        #endregion
    }
}