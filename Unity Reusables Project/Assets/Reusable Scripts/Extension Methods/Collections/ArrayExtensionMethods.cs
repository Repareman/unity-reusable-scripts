﻿#region Disclaimer

// -----------------------------------------------------------------------
// <copyright file="ArrayExtensionMethods.cs">
// Copyright (c) 2016, Sam Bolton
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
// IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// -----------------------------------------------------------------------

#endregion

namespace ReusableScripts.ExtensionMethods.Collections
{
    using System;

    /// <summary>
    ///     A collection of extension methods used generically for array objects.
    /// </summary>
    public static class ArrayExtensionMethods
    {
        #region  Public Methods

        /// <summary>
        ///     Shuffles this array.
        /// </summary>
        /// <typeparam name="T">The type of objects in the array.</typeparam>
        /// <param name="array">The array element.</param>
        /// <remarks>
        ///     Implements the Fisher-Yates shuffle algorithm. It is unbiased and provides better results. Is an O(n)
        ///     implementation.
        /// </remarks>
        public static void Shuffle<T>(this T[] array)
        {
            // Create the randomizer that will be needed
            Random random = new Random();

            // Run the algorithm
            for (int i = array.Length - 1; i > 0; i--)
            {
                // Get a random integer with a maximum of i
                int r = random.Next(i);

                // Swap the element at r with the element at i
                array.Swap(r, i);
            }
        }

        /// <summary>
        ///     Swaps the specified indices elements with one another.
        /// </summary>
        /// <typeparam name="T">The type of objects contained in the array.</typeparam>
        /// <param name="array">The array that the elements will be swapped inside of</param>
        /// <param name="indexOne">The index of the first element to swap.</param>
        /// <param name="indexTwo">The index of the second element to swap.</param>
        /// <exception cref="ArgumentOutOfRangeException">
        ///     indexOne;The first index provided is out of the range of the array provided.
        ///     or
        ///     indexTwo;The second index provided is out of the range of the array provided.
        /// </exception>
        /// <remarks>
        ///     The element at index two will be placed at index one. Then the element that was at index one will be placed at
        ///     index two. This will effectively swap the elements locations in the list.
        /// </remarks>
        public static void Swap<T>(this T[] array, int indexOne, int indexTwo)
        {
            // Check that the first index is in range
            if (indexOne < 0 ||
                indexOne >= array.Length)
            {
                throw new ArgumentOutOfRangeException(
                    "indexOne",
                    "The first index provided is out of the range of the array provided.");
            }

            // Check that the second index is in range
            if (indexTwo < 0 ||
                indexTwo >= array.Length)
            {
                throw new ArgumentOutOfRangeException(
                    "indexTwo",
                    "The second index provided is out of the range of the array provided.");
            }

            // Swap the items
            T temp = array[indexOne];
            array[indexOne] = array[indexTwo];
            array[indexTwo] = temp;
        }

        #endregion
    }
}