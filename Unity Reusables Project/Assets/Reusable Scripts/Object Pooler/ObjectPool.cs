﻿#region Disclaimer

// -----------------------------------------------------------------------
// <copyright file="ObjectPool.cs">
// Copyright (c) 2016, Sam Bolton
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
// IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// -----------------------------------------------------------------------

#endregion

namespace ReusableScripts.ObjectPooler
{
    using System.Collections.Generic;
    using System.Linq;
    using MonoBehaviours;
    using UnityEngine;

    /// <summary>
    ///     Base instructions for running a pool of game objects.
    /// </summary>
    /// <typeparam name="T">
    ///     The class that is the pooler.
    /// </typeparam>
    public abstract class ObjectPool<T> : AutoGeneratingSingletonMonoBehaviour<T>
        where T : MonoBehaviour
    {
        #region Private Field

        /// <summary>
        ///     The prefab that will be made for the pooler to monitor.
        /// </summary>
        private GameObject _rawPooledObjectPrefab;

        /// <summary>
        ///     List of the pooled objects.
        /// </summary>
        private List<GameObject> _rawPooledObjects;

        #endregion

        #region Public Property

        /// <summary>
        ///     Gets or sets the parent of the object when it is active in the scene.
        /// </summary>
        public abstract Transform ActiveParent { get; set; }

        #endregion

        #region Protected Property

        /// <summary>
        ///     Gets a value indicating whether this pool is allowed to grow or not.
        /// </summary>
        protected abstract bool _grow { get; }

        /// <summary>
        ///     Gets the maximum number of pooled objects that should be allowed to exist in the pool at
        ///     any given moment in time.
        /// </summary>
        /// <remarks>
        ///     Set to 0 if the maximum is infinite.
        /// </remarks>
        protected abstract int _max { get; }

        /// <summary>
        ///     Gets the minimum number of pooled objects that should exist in the pool at any given
        ///     moment in time.
        /// </summary>
        /// <remarks>
        ///     If the minimum is 0, then no objects will be made at the beginning of the scene.
        /// </remarks>
        protected abstract int _min { get; }

        /// <summary>
        ///     Gets or sets the prefab that will be made for the pooler to monitor.
        /// </summary>
        protected GameObject _pooledObjectPrefab
        {
            get
            {
                return _rawPooledObjectPrefab;
            }

            set
            {
                _rawPooledObjectPrefab = value;
                BufferPrefabPool();
            }
        }

        /// <summary>
        ///     Gets the list of pooled objects.
        /// </summary>
        protected List<GameObject> _pooledObjects
        {
            get
            {
                return _rawPooledObjects;
            }
        }

        #endregion

        #region AutoGeneratingSingletonMonoBehaviour<T> Overriden Protected Methods

        /// <summary>
        ///     Called during the awake function.
        /// </summary>
        /// <remarks>
        ///     Use this instead of Awake so that the singleton can remain to operate correctly.
        /// </remarks>
        protected override void SingletonAwake()
        {
            base.SingletonAwake();

            _rawPooledObjects = _grow || _max == 0 ? new List<GameObject>() : new List<GameObject>(_max);
        }

        #endregion

        #region  Public Methods

        /// <summary>
        ///     Gets all the objects in the pool that are currently active.
        /// </summary>
        /// <returns>
        ///     A list of game objects that are currently active in the scene.
        /// </returns>
        public virtual GameObject[] GetAllActivePooledObjects()
        {
            return _rawPooledObjects.Where(x => x.activeInHierarchy).ToArray();
        }

        /// <summary>
        ///     Gets all the objects in the pool that are currently inactive.
        /// </summary>
        /// <returns>
        ///     A list of game objects that are not currently active in the scene.
        /// </returns>
        public virtual GameObject[] GetAllInactivePooledObjects()
        {
            return _rawPooledObjects.Where(x => !x.activeInHierarchy).ToArray();
        }

        /// <summary>
        ///     Gets and activates a pooled object.
        /// </summary>
        /// <returns>The game object that was just activated.</returns>
        public virtual GameObject GetAndActivatePooledObject()
        {
            // Find the first object in the pool that isn't active in the hierarchy 
            GameObject returnObject = _rawPooledObjects.FirstOrDefault(x => !x.activeInHierarchy);

            // If the return object isn't null, return it 
            if (returnObject != null)
            {
                ActivatePooledObject(returnObject);
                return returnObject;
            }

            // Compensate for there being no max 
            bool maxReached = _max != 0 && _rawPooledObjects.Count >= _max;

            // If we can't grow then return a null reference
            if (!_grow || maxReached)
            {
                return null;
            }

            // Make a new pooled object and return it
            returnObject = CreatePooledObject();
            ActivatePooledObject(returnObject);
            return returnObject;
        }

        /// <summary>
        ///     Gets the number of pooled objects and activates them according to <see cref="ActivatePooledObject" />.
        /// </summary>
        /// <param name="amount">The number of objects to get.</param>
        /// <returns>An array of the objects that were retrieved.</returns>
        public virtual GameObject[] GetAndActivatePooledObjects(int amount)
        {
            // Get the first number of non-active game objects in the pool up to the desired amount
            List<GameObject> returnedGameObjects =
                _rawPooledObjects.Where(x => !x.activeInHierarchy).Take(amount).ToList();

            // If this was the number of game objects needed then it returns the list 
            if (returnedGameObjects.Count >= amount)
            {
                // Set each game object
                foreach (GameObject curGameObject in returnedGameObjects)
                {
                    ActivatePooledObject(curGameObject);
                }

                // Return the list of game objects 
                return returnedGameObjects.ToArray();
            }

            // If more are required then generate them 
            while (!(_max != 0 && _rawPooledObjects.Count > _max) &&
                   _grow &&
                   returnedGameObjects.Count < amount)
            {
                GameObject newObject = CreatePooledObject();
                newObject.transform.SetParent(ActiveParent);
                returnedGameObjects.Add(newObject);
            }

            // Set each game object
            foreach (GameObject curGameObject in returnedGameObjects)
            {
                ActivatePooledObject(curGameObject);
            }

            // Return the list of game objects 
            return returnedGameObjects.ToArray();
        }

        /// <summary>
        ///     Call to get a game object from the pooler.
        /// </summary>
        /// <returns>
        ///     An inactive object from the pool.
        /// </returns>
        public virtual GameObject GetPooledObject()
        {
            // Find the first object in the pool that isn't active in the hierarchy 
            GameObject returnObject = _rawPooledObjects.FirstOrDefault(x => !x.activeInHierarchy);

            // If the return object isn't null, return it 
            if (returnObject != null)
            {
                return returnObject;
            }

            // Compensate for there being no max 
            bool maxReached = _max != 0 && _rawPooledObjects.Count >= _max;

            // If we can't grow then return a null reference
            if (!_grow || maxReached)
            {
                return null;
            }

            // Make a new pooled object and return it
            returnObject = CreatePooledObject();
            returnObject.transform.SetParent(transform);
            returnObject.SetActive(false);
            return returnObject;
        }

        /// <summary>
        ///     Gets multiple game objects, but does not activate them.
        /// </summary>
        /// <param name="amount">
        ///     The number of game objects you want to retrieve from the pooler.
        /// </param>
        /// <returns>
        ///     An array of game objects that were retrieved from the pooler.
        /// </returns>
        public virtual GameObject[] GetPooledObjects(int amount)
        {
            // Get the first number of non-active game objects in the pool up to the desired amount
            List<GameObject> returnedGameObjects =
                _rawPooledObjects.Where(x => !x.activeInHierarchy).Take(amount).ToList();

            // If this was the number of game objects needed then it returns the list 
            if (returnedGameObjects.Count >= amount)
            {
                // Return the list of game objects 
                return returnedGameObjects.ToArray();
            }

            // If more are required then generate them 
            while (!(_max != 0 && _rawPooledObjects.Count > _max) &&
                   _grow &&
                   returnedGameObjects.Count < amount)
            {
                GameObject newObject = CreatePooledObject();
                newObject.transform.SetParent(transform);
                newObject.SetActive(false);
                returnedGameObjects.Add(newObject);
            }

            // Return the list of game objects 
            return returnedGameObjects.ToArray();
        }

        #endregion

        #region  Protected Methods

        /// <summary>
        ///     Sets the pooled object to its active state.
        /// </summary>
        /// <param name="pooledObject">The pooled object.</param>
        protected abstract void ActivatePooledObject(GameObject pooledObject);

        /// <summary>
        ///     Creates a new instance of the prefab game object.
        /// </summary>
        /// <returns>
        ///     The new instance of the prefab game object.
        /// </returns>
        protected virtual GameObject CreatePooledObject()
        {
            // Create the new instance of the object 
            GameObject newObject = Instantiate(_rawPooledObjectPrefab);

            // Add the instance to the pooled objects list 
            _rawPooledObjects.Add(newObject);

            // Set the pooler variable in the pooled object to this pooler
            newObject.GetComponent<PooledObject<T>>().Pooler = this;

            // Return the instance 
            return newObject;
        }

        #endregion

        #region  Private Methods

        /// <summary>
        ///     Buffers the prefab pool with the amount of objects equal to the minimum specified for
        ///     this class.
        /// </summary>
        private void BufferPrefabPool()
        {
            for (int i = 0; i < _min; i++)
            {
                // Create the new instance of the object 
                GameObject newBuffer = CreatePooledObject();

                // Set the object to not active 
                newBuffer.SetActive(false);

                // Set the parent of the game object to the object poolers object 
                newBuffer.transform.SetParent(transform);
            }
        }

        #endregion
    }
}