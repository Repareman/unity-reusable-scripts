﻿#region Disclaimer

// -----------------------------------------------------------------------
// <copyright file="PersistentAutoGeneratingSingletonMonoBehaviour.cs">
// Copyright (c) 2016, Sam Bolton
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
// IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// -----------------------------------------------------------------------

#endregion

namespace ReusableScripts.MonoBehaviours
{
    using UnityEngine;

    /// <summary>
    ///     Provides singleton functionality that is automatically generated when referenced for the
    ///     class that will persist throughout scenes.
    /// </summary>
    /// <typeparam name="T">
    ///     The class type that is going to be a persistent auto generating singleton.
    /// </typeparam>
    public abstract class PersistentAutoGeneratingSingletonMonoBehaviour<T> : AutoGeneratingSingletonMonoBehaviour<T>
        where T : MonoBehaviour
    {
        #region AutoGeneratingSingletonMonoBehaviour<T> Overriden Protected Methods

        /// <summary>
        ///     Called during the awake function.
        /// </summary>
        /// <remarks>
        ///     Use this instead of Awake so that the singleton can remain to operate correctly.
        /// </remarks>
        protected override void SingletonAwake()
        {
            // Make this persistent 
            DontDestroyOnLoad(gameObject);
        }

        #endregion
    }
}