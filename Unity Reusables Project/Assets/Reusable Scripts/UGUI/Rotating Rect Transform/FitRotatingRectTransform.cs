﻿#region Disclaimer

// -----------------------------------------------------------------------
// <copyright file="FitRotatingRectTransform.cs">
// Copyright (c) 2016, Sam Bolton
// All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
// IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
// -----------------------------------------------------------------------

#endregion

namespace ReusableScripts.UGUI
{
    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;

    /// <summary>
    ///     Fits the object to the size of its parent and rotates it inside of those bounds.
    /// </summary>
    /// <seealso cref="UIBehaviour" />
    /// <seealso cref="ILayoutSelfController" />
    [ExecuteInEditMode, RequireComponent(typeof(RectTransform))]
    public class FitRotatingRectTransform : UIBehaviour, ILayoutSelfController
    {
        #region Private Field

        /// <summary>
        ///     The amount of offset to give the item in the height.
        /// </summary>
        [SerializeField]
        private float _heightOffset = 0;

        /// <summary>
        ///     Reference to the rect transform of the parent object.
        /// </summary>
        private RectTransform _parentRectTransform;

        /// <summary>
        ///     Reference to the rect transform of this object.
        /// </summary>
        private RectTransform _rectTransform;

        /// <summary>
        ///     Drives the rect transform of the object and how it is displayed.
        /// </summary>
        private DrivenRectTransformTracker _rectTransformTracker = new DrivenRectTransformTracker();

        /// <summary>
        ///     The amount to rotate this object.
        /// </summary>
        [SerializeField]
        private float _rotation = 0;

        /// <summary>
        ///     The amount of offset to give the item in the width.
        /// </summary>
        [SerializeField]
        private float _widthOffset = 0;

        #endregion

        #region UIBehaviour Overriden Protected Methods

        /// <summary>
        ///     See MonoBehaviour.OnDisable.
        /// </summary>
        protected override void OnDisable()
        {
            base.OnDisable();

            // Clear out the rect transform tracker if this is disabled.
            _rectTransformTracker.Clear();
        }

        /// <summary>
        ///     See MonoBehaviour.OnEnable.
        /// </summary>
        protected override void OnEnable()
        {
            base.OnEnable();

            // Get RectTransform references
            _rectTransform = GetComponent<RectTransform>();
            _parentRectTransform = transform.parent.GetComponent<RectTransform>();

            // Setup the RectTransform driver
            _rectTransformTracker.Add(gameObject, _rectTransform, DrivenTransformProperties.All);
            _rectTransformTracker.Add(gameObject, _rectTransform, DrivenTransformProperties.AnchorMax);
            _rectTransformTracker.Add(gameObject, _rectTransform, DrivenTransformProperties.AnchorMin);
        }

        #endregion

        #region  Private Methods

        /// <summary>
        ///     Calculates the size of the item based on the given rotation.
        /// </summary>
        /// <returns>The size of the object.</returns>
        private Vector2 CalculateItemSize()
        {
            // Convert the rotation to radians
            float radianRotation = Mathf.Abs(_rotation % 360) * Mathf.Deg2Rad;

            // Normalize the given rotation for easier calculations
            if (radianRotation <= Mathf.PI &&
                radianRotation > Mathf.PI / 2)
            {
                radianRotation = Mathf.PI - radianRotation;
            }
            else if (radianRotation <= Mathf.PI * 1.5f &&
                     radianRotation > Mathf.PI)
            {
                radianRotation = radianRotation - Mathf.PI;
            }
            else if (radianRotation <= 2 * Mathf.PI &&
                     radianRotation > Mathf.PI * 1.5f)
            {
                radianRotation = 2 * Mathf.PI - radianRotation;
            }

            // Pre-calculate the width and height of the parent object
            float parentWidth = _parentRectTransform.sizeDelta.x + _widthOffset;
            float parentHeight = _parentRectTransform.sizeDelta.y + _heightOffset;

            // Calculate the two possible heights
            float heightOne = parentHeight * parentHeight /
                              (parentWidth * Mathf.Sin(radianRotation) + parentHeight * Mathf.Cos(radianRotation));
            float heightTwo = parentHeight * parentWidth /
                              (parentWidth * Mathf.Cos(radianRotation) + parentHeight * Mathf.Sin(radianRotation));

            // Find the actual height
            float returnHeight = Mathf.Min(heightOne, heightTwo);

            // Calculate the width based on the height
            float returnWidth = returnHeight * parentWidth / parentHeight;

            // Return the calculated values
            return new Vector2(returnWidth, returnHeight);
        }

        /// <summary>
        ///     Sets the rect transform values.
        /// </summary>
        private void SetRectTransformValues()
        {
            _rectTransform.localRotation = Quaternion.Euler(0, 0, _rotation);
            _rectTransform.sizeDelta = CalculateItemSize();
            _rectTransform.anchoredPosition = Vector2.zero;
            _rectTransform.anchorMax = Vector2.one * 0.5f;
            _rectTransform.anchorMin = Vector2.one * 0.5f;
        }

        /// <summary>
        ///     Updates this instance.
        /// </summary>
        private void Update()
        {
            if (Application.isPlaying)
            {
                return;
            }

            SetRectTransformValues();
        }

        #endregion

        #region  ILayoutController Implementation

        /// <summary>
        ///     Sets the layout horizontal.
        /// </summary>
        public void SetLayoutHorizontal()
        {
            SetRectTransformValues();
        }

        /// <summary>
        ///     Sets the layout vertical.
        /// </summary>
        public void SetLayoutVertical()
        {
            SetRectTransformValues();
        }

        #endregion
    }
}